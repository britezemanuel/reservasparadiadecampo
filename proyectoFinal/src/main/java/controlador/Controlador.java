package controlador;

import java.util.List;
import modelo.*;
import persistencia.ControladorPersistencia;

public class Controlador {
    private ControladorPersistencia cp;

    public Controlador() {
        this.cp=new ControladorPersistencia();
    }
//-----------------------------------------------------PERSISTENCIAS---------------------------------------------------------------
    public void persistirCliente(Cliente unCliente){
        this.cp.persistirCliente(unCliente);
    }
    public void persistirIngrediente(Ingrediente unIngrediente){
        this.cp.persistirIngrediente(unIngrediente);
    }
    public void persistirMenu(Menu unMenu){
        this.cp.persistirMenu(unMenu);
    }
    public void persistirReserva(Reserva unaReserva){
        this.cp.persistirReserva(unaReserva);
    }
    
//-----------------------------------------------------ACTUALIZAR---------------------------------------------------------------
    public void actualizarCliente(Cliente unCliente){
        this.cp.actualizarCliente(unCliente);
    }
    public void actualizarIngrediente(Ingrediente unIngrediente){
        this.cp.actualizarIngrediente(unIngrediente);
    }
    public void actualizarMenu(Menu unMenu){
        this.cp.actualizarMenu(unMenu);
    }
    public void actualizarReserva(Reserva unaReserva){
        this.cp.actualizarReserva(unaReserva);
    }
    
//-----------------------------------------------------ELIMINAR---------------------------------------------------------------    
    public void eliminarCliente(Cliente unCliente){
        this.cp.eliminarCliente(unCliente);
    }
    public void eliminarIngrediente(Ingrediente unIngrediente){
        this.cp.eliminarIngrediente(unIngrediente);
    }
    public void eliminarMenu(Menu unMenu){
        this.cp.eliminarMenu(unMenu);
    }
    public void eliminarReserva(Reserva unaReserva){
        this.cp.eliminarReserva(unaReserva);
    }
    

//-----------------------------------------------------LISTAR---------------------------------------------------------------    

    public List<Cliente> ListarClientes(){
        List<Cliente> clientes = this.cp.ListarClientes();
        return clientes;
    }
    
    public List<Cliente> ListarClientesAlfabeticamente(){
        List<Cliente> clientes = this.cp.ListarClientesAlfabeticamente();
        return clientes;
    }    

    public List<Ingrediente> ListarIngredientesDelMenu(int id){
        List<Ingrediente> ingredientes = this.cp.ListarIngredientesDelMenu(id);
        return ingredientes;
    }

    public List<Ingrediente> ListarIngredientes(){
        List<Ingrediente> ingredientes = this.cp.ListarIngredientes();
        return ingredientes;
    }    

    public List<Menu> ListarMenus(){
        List<Menu> menus = this.cp.ListarMenus();
        return menus;
    }    

    public List<Reserva> ListarReservas(){
        List<Reserva> reservas = this.cp.ListarReservas();
        return reservas;
    }

//-----------------------------------------------------BUSCAR---------------------------------------------------------------    

    public Cliente buscarCliente (int id){
        Cliente cli = this.cp.BuscarCliente(id);
        return cli;
    }
    public Cliente buscarCliente (String nombre){
        Cliente cli = this.cp.BuscarCliente(nombre);
        return cli;
    }
    public Ingrediente buscarIngrediente (int id){
        Ingrediente ingre = this.cp.BuscarIngrediente(id);
        return ingre;
    }
    public Ingrediente buscarIngrediente (String nombre){
        Ingrediente ingre = this.cp.BuscarIngrediente(nombre);
        return ingre;
    }
    public Menu buscarMenu (int id){
        Menu menu = this.cp.BuscarMenu(id);
        return menu;
    }
    public Menu buscarMenu (String nombre){
        Menu menu = this.cp.BuscarMenu(nombre);
        return menu;
    }
    public Reserva buscarReserva (int id){
        Reserva res = this.cp.BuscarReserva(id);
        return res;
    }
    
//-----------------------------------------------------CONTROLADORES DE DATOS DE ENTRADA---------------------------------------------------------------    

    public int ControlarCajaTextoLetras (String texto){
        int ban=0;
        if (texto.isEmpty() == true){
            ban = 1;
        }else{
            char[] caracteres = texto.toCharArray();
            for(int i=0; i< caracteres.length; i++){
                if( Character.isDigit(caracteres[i]) == true){
                    ban=1;
                }
            }
        }
        return ban;
    }
    public int ControlarCajaTextoVacia (String texto){
        int ban=0;
        if (texto.isEmpty() == true){
            ban = 1;
        }
        return ban;
    }
}
