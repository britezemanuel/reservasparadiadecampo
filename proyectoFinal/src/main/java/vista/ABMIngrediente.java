package vista;
//-----------------------------------------------------LIBRERIAS---------------------------------------------------------------   
import controlador.Controlador;
import java.awt.Event;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.InputMap;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import modelo.Ingrediente;

public class ABMIngrediente extends javax.swing.JPanel {
    private Controlador controlador;
    public ABMIngrediente() {
        initComponents();
        //ESTAS LINEAS DE ABAJO EVITAN QUE SE PUEDA PEGAR DENTRO DE LAS CAJAS DE TEXTO
        InputMap map1 = txtId.getInputMap(txtId.WHEN_FOCUSED);
        map1.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
        InputMap map2 = txtNombre.getInputMap(txtNombre.WHEN_FOCUSED);
        map2.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
    }

    public ABMIngrediente(Controlador controlador) {
        this();
        this.controlador = controlador;
        this.cargarTabla();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        txtId = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        botonBuscar = new javax.swing.JButton();
        botonModificar = new javax.swing.JButton();
        botonNuevo = new javax.swing.JButton();
        botonGuardar = new javax.swing.JButton();
        botonAgregar = new javax.swing.JButton();
        botonEliminar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaIngredientes = new javax.swing.JTable();

        jPanel1.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 945, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 945, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        txtId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtIdKeyTyped(evt);
            }
        });

        txtNombre.setEnabled(false);
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        botonBuscar.setText("BUSCAR");
        botonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarActionPerformed(evt);
            }
        });

        botonModificar.setText("MODIFICAR");
        botonModificar.setEnabled(false);
        botonModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonModificarActionPerformed(evt);
            }
        });

        botonNuevo.setText("NUEVO");
        botonNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNuevoActionPerformed(evt);
            }
        });

        botonGuardar.setText("GUARDAR");
        botonGuardar.setEnabled(false);
        botonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarActionPerformed(evt);
            }
        });

        botonAgregar.setText(" AGREGAR");
        botonAgregar.setEnabled(false);
        botonAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarActionPerformed(evt);
            }
        });

        botonEliminar.setText("ELIMINAR");
        botonEliminar.setEnabled(false);
        botonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarActionPerformed(evt);
            }
        });

        jLabel1.setText("ID");

        jLabel2.setText("Nombre");

        tablaIngredientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        tablaIngredientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaIngredientesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaIngredientes);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                    .addComponent(botonGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonAgregar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(41, 41, 41)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(199, 199, 199))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(botonBuscar))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(botonModificar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(botonGuardar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(botonEliminar))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(botonNuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(botonAgregar)))
                .addContainerGap(158, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

//-----------------------------------------------------BOTONES-----------------------------------------------------------------------   
    
    private void botonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarActionPerformed
        int id = Integer.parseInt(txtId.getText());
        Ingrediente ing = this.controlador.buscarIngrediente(id);
        if (ing == null){
            JOptionPane.showMessageDialog(this, "ID NO ENCONTRADO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            this.controlador.eliminarIngrediente(ing);
            JOptionPane.showMessageDialog(this, "INGREDIENTE ELIMINADO EXITOSAMENTE.", "Aviso", INFORMATION_MESSAGE);
        }
        resetear();
    }//GEN-LAST:event_botonEliminarActionPerformed

    private void botonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarActionPerformed
        if ("".equals(txtNombre.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO NOMBRE VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            String nombre = txtNombre.getText();
            Ingrediente ing = this.controlador.buscarIngrediente(nombre);
            if (ing != null){
                JOptionPane.showMessageDialog(this, "EL INGREDIENTE YA EXISTE", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                ing = new Ingrediente (nombre, "VISIBLE");
                this.controlador.persistirIngrediente(ing);
                JOptionPane.showMessageDialog(this, "INGREDIENTE CARGADO EXITOSAMENTE", "Aviso", INFORMATION_MESSAGE);
            }
        }
        resetear();
    }//GEN-LAST:event_botonAgregarActionPerformed

    private void botonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarActionPerformed
        if ("".equals(txtNombre.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO NOMBRE VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            String nombre = txtNombre.getText();
            int id = Integer.parseInt(txtId.getText());
            Ingrediente ing = this.controlador.buscarIngrediente(id);
            if (ing != null){
                ing.setNombre(nombre);
                this.controlador.actualizarIngrediente(ing);
                JOptionPane.showMessageDialog(this, "INGREDIENTE ACTUALIZADO EXITOSAMENTE", "Aviso", INFORMATION_MESSAGE);
            }
        }
        resetear();
    }//GEN-LAST:event_botonGuardarActionPerformed

    private void botonNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNuevoActionPerformed
        txtId.setEnabled(false);
        txtNombre.setEnabled(true);
        txtId.setText("");
        txtNombre.setText("");
        botonBuscar.setEnabled(false);
        botonModificar.setEnabled(false);
        botonNuevo.setEnabled(false);
        botonEliminar.setEnabled(false);
        botonAgregar.setEnabled(true);
    }//GEN-LAST:event_botonNuevoActionPerformed

    private void botonModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonModificarActionPerformed
        txtId.setEnabled(false);
        txtNombre.setEnabled(true);
        botonBuscar.setEnabled(false);
        botonModificar.setEnabled(false);
        botonGuardar.setEnabled(true);
        botonEliminar.setEnabled(false);
        botonNuevo.setEnabled(false);
    }//GEN-LAST:event_botonModificarActionPerformed

    private void botonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarActionPerformed
        if ("".equals(txtId.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO ID VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            int id = Integer.parseInt(txtId.getText());
            Ingrediente ing = this.controlador.buscarIngrediente(id);
            if(ing == null){
                JOptionPane.showMessageDialog(this, "ID NO ENCONTRADO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                txtNombre.setText(ing.getNombre());
                botonModificar.setEnabled(true);
                botonEliminar.setEnabled(true);
            }
        }
    }//GEN-LAST:event_botonBuscarActionPerformed

//-----------------------------------------------------CAJAS DE TEXTO---------------------------------------------------------------   

    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        char c=evt.getKeyChar();
        if(Character.isLowerCase(c)){
            String cad=(""+c).toUpperCase();
            c=cad.charAt(0);
            evt.setKeyChar(c);
        }
    }//GEN-LAST:event_txtNombreKeyTyped

    private void txtIdKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIdKeyTyped
        char caracter = evt.getKeyChar();
        if(!(((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)))
        {
            String cad=(""+caracter);
            caracter=cad.charAt(0);
            evt.setKeyChar(caracter);
        }else{
            evt.consume();
        }

    }//GEN-LAST:event_txtIdKeyTyped

    private void tablaIngredientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaIngredientesMouseClicked
        // TODO add your handling code here:
        int nfilas=0;
        int fila=0;
        nfilas=tablaIngredientes.getSelectedRowCount(); //Obtiene el numero de filas seleccionadas
        if(nfilas==1){
            fila=tablaIngredientes.getSelectedRow();
            int idIngrediente=Integer.parseInt((String)tablaIngredientes.getValueAt(fila, 0).toString());
            txtId.setText(""+idIngrediente);
            String nombre=(String)tablaIngredientes.getValueAt(fila, 1).toString();
            txtNombre.setText(""+nombre);
            txtId.setEnabled(true);
            txtNombre.setEnabled(false);
            botonBuscar.setEnabled(true);
            botonModificar.setEnabled(true);
            botonNuevo.setEnabled(true);
            botonAgregar.setEnabled(false);
            botonEliminar.setEnabled(true);
            botonGuardar.setEnabled(false);
        }
    }//GEN-LAST:event_tablaIngredientesMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAgregar;
    private javax.swing.JButton botonBuscar;
    private javax.swing.JButton botonEliminar;
    private javax.swing.JButton botonGuardar;
    private javax.swing.JButton botonModificar;
    private javax.swing.JButton botonNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaIngredientes;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables

//-----------------------------------------------------FUNCIONES---------------------------------------------------------------   

    public void cargarTabla(){
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("IDINGREDIENTE");//0
        modelo.addColumn("NOMBRE");//1
      
        List<Ingrediente> ingredientes = this.controlador.ListarIngredientes();
        
        modelo.setRowCount(ingredientes.size());
        
        for(int i = 0; i < ingredientes.size(); i++){
            
            modelo.setValueAt(ingredientes.get(i).getIdIngrediente(), i, 0);
            modelo.setValueAt(ingredientes.get(i).getNombre(), i, 1);
          
        }
        this.tablaIngredientes.setModel(modelo);
    }
    
    public void resetear(){
        txtId.setEnabled(true);
        txtNombre.setEnabled(false);
        txtId.setText("");
        txtNombre.setText("");
        botonBuscar.setEnabled(true);
        botonModificar.setEnabled(false);
        botonGuardar.setEnabled(false);
        botonEliminar.setEnabled(false);
        botonAgregar.setEnabled(false);
        botonNuevo.setEnabled(true);
        this.cargarTabla();
    }

}
