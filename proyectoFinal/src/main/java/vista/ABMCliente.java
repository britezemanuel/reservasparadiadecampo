package vista;
//-----------------------------------------------------LIBRERIAS---------------------------------------------------------------   
import controlador.Controlador;
import java.awt.Event;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.InputMap;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import modelo.Cliente;

public class ABMCliente extends javax.swing.JPanel {
    private Controlador controlador;
    public ABMCliente() {
        initComponents();
        //ESTAS LINEAS DE ABAJO EVITAN QUE SE PUEDA PEGAR DENTRO DE LAS CAJAS DE TEXTO
        InputMap map1 = txtId.getInputMap(txtId.WHEN_FOCUSED);
        map1.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
        InputMap map2 = txtDocumento.getInputMap(txtDocumento.WHEN_FOCUSED);
        map2.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
        InputMap map3 = txtTelefono.getInputMap(txtTelefono.WHEN_FOCUSED);
        map3.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
    }

    public ABMCliente(Controlador controlador) {
        this();
        this.controlador = controlador;
        this.cargarTabla();
    }    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtApellido = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtDocumento = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtDomicilio = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtTelefono = new javax.swing.JTextField();
        botonBuscar = new javax.swing.JButton();
        botonModificar = new javax.swing.JButton();
        botonGuardar = new javax.swing.JButton();
        botonEliminar = new javax.swing.JButton();
        botonNuevo = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaClientes = new javax.swing.JTable();
        botonAgregar = new javax.swing.JButton();

        jPanel1.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 900, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 900, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        jLabel1.setText("ID");

        txtId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtIdKeyTyped(evt);
            }
        });

        jLabel2.setText("Apellido");

        txtApellido.setEnabled(false);
        txtApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoKeyTyped(evt);
            }
        });

        jLabel3.setText("Nombre");

        txtNombre.setEnabled(false);
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        jLabel4.setText("Documento");

        txtDocumento.setEnabled(false);
        txtDocumento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDocumentoKeyTyped(evt);
            }
        });

        jLabel5.setText("Domicilio");

        txtDomicilio.setEnabled(false);
        txtDomicilio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDomicilioKeyTyped(evt);
            }
        });

        jLabel6.setText("Telefono");

        txtTelefono.setEnabled(false);
        txtTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoKeyTyped(evt);
            }
        });

        botonBuscar.setText("BUSCAR");
        botonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarActionPerformed(evt);
            }
        });

        botonModificar.setText("MODIFICAR");
        botonModificar.setEnabled(false);
        botonModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonModificarActionPerformed(evt);
            }
        });

        botonGuardar.setText("GUARDAR");
        botonGuardar.setEnabled(false);
        botonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarActionPerformed(evt);
            }
        });

        botonEliminar.setText("ELIMINAR");
        botonEliminar.setEnabled(false);
        botonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarActionPerformed(evt);
            }
        });

        botonNuevo.setText("NUEVO");
        botonNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNuevoActionPerformed(evt);
            }
        });

        tablaClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "ID", "APELLIDO", "NOMBRE", "DOCUMENTO", "DOMICILIO", "DOMICILIO", "TELEFONO"
            }
        ));
        tablaClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaClientesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaClientes);

        botonAgregar.setText("AGREGAR");
        botonAgregar.setEnabled(false);
        botonAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtId)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtApellido)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtNombre)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                    .addComponent(txtDocumento)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDomicilio)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTelefono))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                    .addComponent(botonGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonAgregar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 515, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(botonBuscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(botonModificar)
                        .addGap(18, 18, 18)
                        .addComponent(botonGuardar)
                        .addGap(18, 18, 18)
                        .addComponent(botonEliminar)
                        .addGap(18, 18, 18)
                        .addComponent(botonNuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(botonAgregar))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtDomicilio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(81, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

//-----------------------------------------------------BOTONES---------------------------------------------------------------   

    private void botonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarActionPerformed
        // TODO add your handling code here
        if ("".equals(txtId.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO ID VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            int id = Integer.parseInt(txtId.getText());
            Cliente ing = this.controlador.buscarCliente(id);
            if(ing == null){
                JOptionPane.showMessageDialog(this, "ID NO ENCONTRADO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                txtApellido.setText(ing.getApellido());
                txtNombre.setText(ing.getNombre());
                txtDocumento.setText(ing.getDocumento());
                txtDomicilio.setText(ing.getDomicilio());
                txtTelefono.setText(ing.getTelefono());
                botonModificar.setEnabled(true);
                botonEliminar.setEnabled(true);
            }
        }
    }//GEN-LAST:event_botonBuscarActionPerformed

    private void botonModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonModificarActionPerformed
        // TODO add your handling code here:
        txtId.setEnabled(false);
        txtApellido.setEnabled(true);
        txtNombre.setEnabled(true);
        txtDocumento.setEnabled(true);
        txtDomicilio.setEnabled(true);
        txtTelefono.setEnabled(true);
        botonBuscar.setEnabled(false);
        botonModificar.setEnabled(false);
        botonGuardar.setEnabled(true);
        botonEliminar.setEnabled(false);
        botonNuevo.setEnabled(false);
    }//GEN-LAST:event_botonModificarActionPerformed

    private void botonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarActionPerformed
        // TODO add your handling code here:
        if ("".equals(txtId.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO ID VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            if ("".equals(txtApellido.getText().replaceAll(" ", ""))){
                JOptionPane.showMessageDialog(this, "CAMPO APELLIDO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                if ("".equals(txtNombre.getText().replaceAll(" ", ""))){
                    JOptionPane.showMessageDialog(this, "CAMPO NOMBRE VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                }else{
                    if ("".equals(txtDocumento.getText().replaceAll(" ", ""))){
                        JOptionPane.showMessageDialog(this, "CAMPO DOCUMENTO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                    }else{
                        if ("".equals(txtDomicilio.getText().replaceAll(" ", ""))){
                            JOptionPane.showMessageDialog(this, "CAMPO DOMICILIO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                        }else{
                            if ("".equals(txtTelefono.getText().replaceAll(" ", ""))){
                                JOptionPane.showMessageDialog(this, "CAMPO TELEFONO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                            }else{
                                int id = Integer.parseInt(txtId.getText());
                                Cliente ing = this.controlador.buscarCliente(id);
                                if (ing != null){
                                    String apellido = txtApellido.getText();
                                    String nombre = txtNombre.getText();
                                    String documento = txtDocumento.getText();
                                    String domicilio = txtDomicilio.getText();
                                    String telefono = txtTelefono.getText();
                                    ing.setApellido(apellido);
                                    ing.setNombre(nombre);
                                    ing.setDocumento(documento);
                                    ing.setDomicilio(domicilio);
                                    ing.setTelefono(telefono);
                                    this.controlador.actualizarCliente(ing);
                                    JOptionPane.showMessageDialog(this, "El Cliente fue actualizado existosamente ", "Aviso", INFORMATION_MESSAGE);
                                    this.cargarTabla();
                                }
                            }
                        }
                    }
                }
            }
        }
        resetear();
    }//GEN-LAST:event_botonGuardarActionPerformed

    private void botonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarActionPerformed
        // TODO add your handling code here:
        if("".equals(txtId.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO ID VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            int id = Integer.parseInt(txtId.getText());
            Cliente ing = this.controlador.buscarCliente(id);
            if (ing == null){
                JOptionPane.showMessageDialog(this, "ID NO ENCONTRADO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                this.controlador.eliminarCliente(ing);
                JOptionPane.showMessageDialog(this, "CLIENTE ELIMINADO EXITOSAMENTE", "Aviso", INFORMATION_MESSAGE);
                this.cargarTabla();
            }
        }
        resetear();
    }//GEN-LAST:event_botonEliminarActionPerformed

    private void botonNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNuevoActionPerformed
        txtId.setEnabled(false);
        txtApellido.setEnabled(true);
        txtNombre.setEnabled(true);
        txtDocumento.setEnabled(true);
        txtDomicilio.setEnabled(true);
        txtTelefono.setEnabled(true);
        txtId.setText("");
        txtApellido.setText("");
        txtNombre.setText("");
        txtDocumento.setText("");
        txtDomicilio.setText("");
        txtTelefono.setText("");
        botonAgregar.setEnabled(true);
        botonNuevo.setEnabled(false);
        botonEliminar.setEnabled(false);
        botonModificar.setEnabled(false);
        botonBuscar.setEnabled(false);
    }//GEN-LAST:event_botonNuevoActionPerformed

    private void botonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarActionPerformed
        if("".equals(txtApellido.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO APELLIDO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            if("".equals(txtNombre.getText().replaceAll(" ", ""))){
                JOptionPane.showMessageDialog(this, "CAMPO NOMBRE VACIO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                if("".equals(txtDocumento.getText().replaceAll(" ", ""))){
                    JOptionPane.showMessageDialog(this, "CAMPO DOCUMENTO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                }else{
                    if("".equals(txtDomicilio.getText().replaceAll(" ", ""))){
                        JOptionPane.showMessageDialog(this, "CAMPO DOMICILIO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                    }else{
                        if("".equals(txtTelefono.getText().replaceAll(" ", ""))){
                            JOptionPane.showMessageDialog(this, "CAMPO TELEFONO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                        }else{
                            String nombre = txtNombre.getText();
                            String apellido = txtApellido.getText();
                            String documento = txtDocumento.getText();
                            String domicilio = txtDomicilio.getText();
                            String telefono = txtTelefono.getText();
                            Cliente ing = this.controlador.buscarCliente(nombre);
                            if (ing != null){
                                JOptionPane.showMessageDialog(this, "EL CLIENTE YA SE ENCUENTRA NE LA BASE DE DATOS", "Error", JOptionPane.ERROR_MESSAGE);
                            }else{
                                ing = new Cliente (apellido, nombre, documento, domicilio, telefono, "VISIBLE");
                                this.controlador.persistirCliente(ing);
                                JOptionPane.showMessageDialog(this, "CLIENTE CARGADO EXITOSAMENTE", "Aviso", INFORMATION_MESSAGE);
                                this.cargarTabla();
                            }
                        }
                    }
                }
            }
        }
        resetear();
    }//GEN-LAST:event_botonAgregarActionPerformed

//-----------------------------------------------------CAJAS DE TEXTO---------------------------------------------------------------   

    private void txtIdKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIdKeyTyped
        // TODO add your handling code here:
        char caracter = evt.getKeyChar();
        if(!(((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)))
        {
            String cad=(""+caracter);
            caracter=cad.charAt(0);
            evt.setKeyChar(caracter);
        }else{
            evt.consume();
        }
    }//GEN-LAST:event_txtIdKeyTyped

    private void txtApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
        if(Character.isLowerCase(c)){
            String cad=(""+c).toUpperCase();
            c=cad.charAt(0);
            evt.setKeyChar(c);
        }
    }//GEN-LAST:event_txtApellidoKeyTyped

    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
        if(Character.isLowerCase(c)){
            String cad=(""+c).toUpperCase();
            c=cad.charAt(0);
            evt.setKeyChar(c);
        }
    }//GEN-LAST:event_txtNombreKeyTyped

    private void txtDocumentoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDocumentoKeyTyped
        // TODO add your handling code here:
        char caracter = evt.getKeyChar();
        if(!(((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)))
        {
            String cad=(""+caracter);
            caracter=cad.charAt(0);
            evt.setKeyChar(caracter);
        }else{
            evt.consume();
        }
    }//GEN-LAST:event_txtDocumentoKeyTyped

    private void txtDomicilioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDomicilioKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
        if(Character.isLowerCase(c)){
            String cad=(""+c).toUpperCase();
            c=cad.charAt(0);
            evt.setKeyChar(c);
        }
    }//GEN-LAST:event_txtDomicilioKeyTyped

    private void txtTelefonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoKeyTyped
        // TODO add your handling code here:
        char caracter = evt.getKeyChar();
        if(!(((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)))
        {
            String cad=(""+caracter);
            caracter=cad.charAt(0);
            evt.setKeyChar(caracter);
        }else{
            evt.consume();
        }
    }//GEN-LAST:event_txtTelefonoKeyTyped

    private void tablaClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaClientesMouseClicked
        // TODO add your handling code here:
        int nfilas=0;
        int fila=0;
        nfilas=tablaClientes.getSelectedRowCount(); //Obtiene el numero de filas seleccionadas
        if(nfilas==1){
            fila=tablaClientes.getSelectedRow();
            int idIngrediente=Integer.parseInt((String)tablaClientes.getValueAt(fila, 0).toString());
            txtId.setText(""+idIngrediente);
            String apellido=(String)tablaClientes.getValueAt(fila, 1).toString();
            txtApellido.setText(""+apellido);
            String nombre=(String)tablaClientes.getValueAt(fila, 2).toString();
            txtNombre.setText(""+nombre);
            String documento=(String)tablaClientes.getValueAt(fila, 3).toString();
            txtDocumento.setText(""+documento);
            String domicilio=(String)tablaClientes.getValueAt(fila, 4).toString();
            txtDomicilio.setText(""+domicilio);
            String telefono=(String)tablaClientes.getValueAt(fila, 5).toString();
            txtTelefono.setText(""+telefono);
            txtId.setEnabled(true);
            txtApellido.setEnabled(false);
            txtNombre.setEnabled(false);
            txtDocumento.setEnabled(false);
            txtDomicilio.setEnabled(false);
            txtTelefono.setEnabled(false);
            botonNuevo.setEnabled(true);
            botonAgregar.setEnabled(false);
            botonGuardar.setEnabled(false);
            botonGuardar.setEnabled(false);
            botonModificar.setEnabled(true);
            botonEliminar.setEnabled(true);
        }   
        
    }//GEN-LAST:event_tablaClientesMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAgregar;
    private javax.swing.JButton botonBuscar;
    private javax.swing.JButton botonEliminar;
    private javax.swing.JButton botonGuardar;
    private javax.swing.JButton botonModificar;
    private javax.swing.JButton botonNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaClientes;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtDocumento;
    private javax.swing.JTextField txtDomicilio;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtTelefono;
    // End of variables declaration//GEN-END:variables

//-----------------------------------------------------FUNCIONES---------------------------------------------------------------   
    public void cargarTabla(){
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("IDMENU");//0
        modelo.addColumn("APELLIDO");//1
        modelo.addColumn("APELLIDO");//3
        modelo.addColumn("DOCUMENTO");//4
        modelo.addColumn("DOMICILIO");//5
        modelo.addColumn("TELEFONO");//6
        List<Cliente> clientes = this.controlador.ListarClientesAlfabeticamente();
        
        modelo.setRowCount(clientes.size());
        
        for(int i = 0; i < clientes.size(); i++){
            modelo.setValueAt(clientes.get(i).getIdCliente(), i, 0);
            modelo.setValueAt(clientes.get(i).getApellido(), i, 1);
            modelo.setValueAt(clientes.get(i).getNombre(), i, 2);
            modelo.setValueAt(clientes.get(i).getDocumento(), i, 3);
            modelo.setValueAt(clientes.get(i).getDomicilio(), i, 4);
            modelo.setValueAt(clientes.get(i).getTelefono(), i, 5);
        }
        this.tablaClientes.setModel(modelo);
    }
    
    public void resetear(){
        txtId.setText("");
        txtApellido.setText("");
        txtNombre.setText("");
        txtDocumento.setText("");
        txtDomicilio.setText("");
        txtTelefono.setText("");
        txtId.setEnabled(true);
        txtApellido.setEnabled(false);
        txtNombre.setEnabled(false);
        txtDocumento.setEnabled(false);
        txtDomicilio.setEnabled(false);
        txtTelefono.setEnabled(false);
        botonGuardar.setEnabled(false);
        botonBuscar.setEnabled(true);
        botonNuevo.setEnabled(true);
        botonAgregar.setEnabled(false);
        botonEliminar.setEnabled(false);
        botonModificar.setEnabled(false);
    }


}
