package vista;
//-----------------------------------------------------LIBRERIAS---------------------------------------------------------------   
import controlador.Controlador;
import java.awt.Event;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.InputMap;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import modelo.Ingrediente;
import modelo.Menu;


public class ABMMenu extends javax.swing.JPanel {
    private Controlador controlador;

    public ABMMenu() {
        initComponents();
        //ESTAS LINEAS DE ABAJO EVITAN QUE SE PUEDA PEGAR DENTRO DE LAS CAJAS DE TEXTO
        InputMap map1 = txtIdMenu.getInputMap(txtIdMenu.WHEN_FOCUSED);
        map1.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
        InputMap map2 = txtPrecio.getInputMap(txtPrecio.WHEN_FOCUSED);
        map2.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
        InputMap map3 = txtIdIngrediente.getInputMap(txtIdIngrediente.WHEN_FOCUSED);
        map3.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
    }

    public ABMMenu(Controlador controlador) {
        this();
        this.controlador = controlador;
        this.cargarTablas();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtIdMenu = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        botonBuscar = new javax.swing.JButton();
        botonModificar = new javax.swing.JButton();
        botonGuardar = new javax.swing.JButton();
        botonEliminar = new javax.swing.JButton();
        botonNuevo = new javax.swing.JButton();
        botonAgregar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaMenus = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaIngredientes = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaIngredientesMenu = new javax.swing.JTable();
        botonAgregarIng = new javax.swing.JButton();
        botonQuitarIng = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtIdIngrediente = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        jPanel1.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 900, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 44, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 900, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        jLabel1.setText("ID");

        txtIdMenu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtIdMenuKeyTyped(evt);
            }
        });

        jLabel2.setText("Nombre");

        txtNombre.setEnabled(false);
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreKeyTyped(evt);
            }
        });

        jLabel3.setText("Precio");

        txtPrecio.setEnabled(false);
        txtPrecio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioKeyTyped(evt);
            }
        });

        botonBuscar.setText("BUSCAR");
        botonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarActionPerformed(evt);
            }
        });

        botonModificar.setText("MODIFICAR");
        botonModificar.setEnabled(false);
        botonModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonModificarActionPerformed(evt);
            }
        });

        botonGuardar.setText("GUARDAR");
        botonGuardar.setEnabled(false);
        botonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarActionPerformed(evt);
            }
        });

        botonEliminar.setText("ELIMINAR");
        botonEliminar.setEnabled(false);
        botonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarActionPerformed(evt);
            }
        });

        botonNuevo.setText("NUEVO");
        botonNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNuevoActionPerformed(evt);
            }
        });

        botonAgregar.setText("AGREGAR");
        botonAgregar.setEnabled(false);
        botonAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarActionPerformed(evt);
            }
        });

        tablaMenus.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nombre", "Precio"
            }
        ));
        tablaMenus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaMenusMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaMenus);

        tablaIngredientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Nombre"
            }
        ));
        tablaIngredientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaIngredientesMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tablaIngredientes);

        tablaIngredientesMenu.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "IDINGREDIENTE ", "NOMBRE"
            }
        ));
        jScrollPane3.setViewportView(tablaIngredientesMenu);

        botonAgregarIng.setText("AGREGAR ING.");
        botonAgregarIng.setEnabled(false);
        botonAgregarIng.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarIngActionPerformed(evt);
            }
        });

        botonQuitarIng.setText("QUITAR ING.");
        botonQuitarIng.setEnabled(false);
        botonQuitarIng.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonQuitarIngActionPerformed(evt);
            }
        });

        jLabel4.setText("ID Ingrediente");

        txtIdIngrediente.setEnabled(false);
        txtIdIngrediente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtIdIngredienteKeyTyped(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel5.setText("Listado de Menus");

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel6.setText("Listado de Ingredientes");

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel7.setText("Ingredientes del Menu Seleccionado");

        jLabel8.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel8.setText("Agregar/Quitar Ingrediente");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtIdMenu)
                                    .addComponent(txtNombre)
                                    .addComponent(txtPrecio))
                                .addGap(22, 22, 22)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(botonAgregar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(botonBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(botonModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                                    .addComponent(botonGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(botonEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(botonNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(28, 28, 28)
                                .addComponent(jLabel8)))
                        .addGap(29, 29, 29))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(botonAgregarIng, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(txtIdIngrediente)
                                .addGap(18, 18, 18)
                                .addComponent(botonQuitarIng, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(32, 32, 32)))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addGap(235, 235, 235))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addComponent(jScrollPane3)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addContainerGap())))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIdMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(botonGuardar))
                        .addGap(11, 11, 11)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                                .addComponent(botonEliminar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(botonNuevo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(35, 35, 35)))
                        .addComponent(botonAgregar)
                        .addGap(51, 51, 51)
                        .addComponent(jLabel8))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(botonBuscar)
                            .addComponent(jLabel5))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(botonModificar))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(38, 38, 38)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botonAgregarIng)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtIdIngrediente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(botonQuitarIng)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(114, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

//-----------------------------------------------------BOTONES---------------------------------------------------------------   

    private void botonAgregarIngActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarIngActionPerformed
        if ("".equals(txtIdMenu.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO ID-MENU VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            if ("".equals(txtIdIngrediente.getText().replaceAll(" ", ""))){
                JOptionPane.showMessageDialog(this, "CAMPO ID-INGREDIENTE VACIO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                int id = Integer.parseInt(txtIdMenu.getText());
                Menu menu = this.controlador.buscarMenu(id);
                if(menu==null){
                    JOptionPane.showMessageDialog(this, "ID DE MENU NO ENCONTRADO", "Error", JOptionPane.ERROR_MESSAGE);
                }else{
                    id=Integer.parseInt(txtIdIngrediente.getText());
                    Ingrediente ing = this.controlador.buscarIngrediente(id);
                    if (ing == null){
                        JOptionPane.showMessageDialog(this, "ID DE INGREDIENTE NO ENCONTRADO", "Error", JOptionPane.ERROR_MESSAGE);                     
                    }else {
                        //cargo una lista con los ingredientes de ese menu y compruebo ccon el valor de ing, si coincide lo quito ya lo tiene
                        int bandera = menu.comprobarIngrediente(ing);
                        if (bandera!=0){
                            //Significa que ya tiene ese ingrediente el menu
                            JOptionPane.showMessageDialog(this, "EL INGREDIENTE YA EXISTE EN ESTE MENU", "Error", ERROR_MESSAGE);
                        }else {
                            menu.agregarIngrediente(ing);
                            this.controlador.actualizarMenu(menu);
                            this.controlador.actualizarIngrediente(ing);
                            id=Integer.parseInt(txtIdMenu.getText());
                            this.cargarTablaIngredientesDelMenu(id);
                            JOptionPane.showMessageDialog(this, "INGREDIENTE AGREGADO", "Aviso", INFORMATION_MESSAGE);
                        }                            
                    }
                }
            }
        }
    }//GEN-LAST:event_botonAgregarIngActionPerformed

    private void botonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarActionPerformed
        if ("".equals(txtIdMenu.getText().replaceAll(" ", ""))){
               JOptionPane.showMessageDialog(this, "CAMPO ID VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            int id = Integer.parseInt(txtIdMenu.getText());
            Menu men = this.controlador.buscarMenu(id);
            if(men == null){
                JOptionPane.showMessageDialog(this, "ID NO ENCONTRADO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                txtNombre.setText(men.getNombre());
                txtPrecio.setText(String.valueOf(men.getPrecio()));
                botonModificar.setEnabled(true);
                botonEliminar.setEnabled(true);
                this.cargarTablaIngredientesDelMenu(id);
            }
        }       
    }//GEN-LAST:event_botonBuscarActionPerformed

    private void botonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarActionPerformed
        if("".equals(txtIdMenu.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO ID VACIO", "Aviso", INFORMATION_MESSAGE);
        }else{
            if("".equals(txtNombre.getText().replaceAll(" ", ""))){
                JOptionPane.showMessageDialog(this, "CAMPO NOMBRE VACIO", "Aviso", INFORMATION_MESSAGE);
            }else{
                if("".equals(txtPrecio.getText().replaceAll(" ", ""))){
                    JOptionPane.showMessageDialog(this, "CAMPO PRECIO VACIO", "Aviso", INFORMATION_MESSAGE);
                }else{
                    Float precio=Float.parseFloat(txtPrecio.getText());
                    if(precio<=0){
                        JOptionPane.showMessageDialog(this, "DEBE INGRESAR UN VALOR SUPERIOR A 0", "Aviso", INFORMATION_MESSAGE);
                    }else{
                        int id = Integer.parseInt(txtIdMenu.getText());
                        Menu men = this.controlador.buscarMenu(id);
                        if(men == null){
                            JOptionPane.showMessageDialog(this, "MENU NO ENCONTRADO", "Aviso", INFORMATION_MESSAGE);
                        }else{
                            String nombre=txtNombre.getText();
                            men.setNombre(nombre);
                            men.setPrecio(precio);
                            this.controlador.actualizarMenu(men);
                            JOptionPane.showMessageDialog(this, "MENU ACTUALIZADO", "Aviso", INFORMATION_MESSAGE);
                            this.cargarTablas();
                        }
                    }
                }
            }
        }
        resetear();
    }//GEN-LAST:event_botonGuardarActionPerformed

    private void botonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarActionPerformed
        if("".equals(txtIdMenu.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO ID VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            int id = Integer.parseInt(txtIdMenu.getText());
            Menu men = this.controlador.buscarMenu(id);
            if (men == null){
                JOptionPane.showMessageDialog(this, "ID NO REGISTRADO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                this.controlador.eliminarMenu(men);
                JOptionPane.showMessageDialog(this, "MENU ELIMINADO", "Aviso", INFORMATION_MESSAGE);
                this.cargarTablas();
            }
        }
        resetear();
    }//GEN-LAST:event_botonEliminarActionPerformed

    private void botonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarActionPerformed
        // TODO add your handling code here:
        if("".equals(txtNombre.getText().replaceAll(" ", ""))){   
            JOptionPane.showMessageDialog(this, "CAMPO NOMBRE VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{   
            if ("".equals(txtPrecio.getText())){
                JOptionPane.showMessageDialog(this, "CAMPO PRECIO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                Float precio = Float.parseFloat(txtPrecio.getText());
                if(precio<=0){
                    JOptionPane.showMessageDialog(this, "DEBE INGRESAR UN VALOR SUPERIOR A 0", "Error", JOptionPane.ERROR_MESSAGE);
                }else{
                    String nombre = txtNombre.getText();
                    Menu men = this.controlador.buscarMenu(nombre);
                    if (men != null){
                        JOptionPane.showMessageDialog(this, "EL MENU INGRESADO YA SE ENCUENTRA CARGADO", "Error", JOptionPane.ERROR_MESSAGE);
                    }else{
                        men = new Menu (nombre, precio, "VISIBLE");
                        this.controlador.persistirMenu(men);
                        JOptionPane.showMessageDialog(this, "MENU CARGADO EXITOSAMENTE", "Aviso", INFORMATION_MESSAGE);
                    }
                }
            }
        }
        resetear();
    }//GEN-LAST:event_botonAgregarActionPerformed

    private void botonQuitarIngActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonQuitarIngActionPerformed
        if ("".equals(txtIdMenu.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO ID MENU VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            if ("".equals(txtIdIngrediente.getText().replaceAll(" ", ""))){
                JOptionPane.showMessageDialog(this, "CAMPO ID INGREDIENTE VACIO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                int id = Integer.parseInt(txtIdMenu.getText());
                Menu menu = this.controlador.buscarMenu(id);
                if(menu==null){
                    JOptionPane.showMessageDialog(this, "MENU NO ENCONTRADO", "Error", JOptionPane.ERROR_MESSAGE);
                }else{
                    id=Integer.parseInt(txtIdIngrediente.getText());
                    Ingrediente ing = this.controlador.buscarIngrediente(id);
                    if (ing == null){
                        JOptionPane.showMessageDialog(this, "INGREDIENTE NO ENCONTRADO", "Error", JOptionPane.ERROR_MESSAGE);                     
                    }else {
                        //cargo una lista con los ingredientes de ese menu y compruebo ccon el valor de ing, si coincide lo quito ya lo tiene
                        int bandera = menu.comprobarIngrediente(ing);
                        if (bandera==0){
                            //Significa que ya tiene ese ingrediente el menu
                            JOptionPane.showMessageDialog(this, "ESTE INGREDIENTE NO SE ENCUENTRA CARGADO EN EL MENU", "Error", ERROR_MESSAGE);
                        }else {
                            menu.quitarIngrediente(ing);
                            this.controlador.actualizarMenu(menu);
                            this.controlador.actualizarIngrediente(ing);
                            id=Integer.parseInt(txtIdMenu.getText());
                            this.cargarTablaIngredientesDelMenu(id);
                            JOptionPane.showMessageDialog(this, "INGREDIENTE QUITADO EXITOSAMENTE", "Aviso", INFORMATION_MESSAGE);
                        }                            
                    }
                }
            }
        }
    }//GEN-LAST:event_botonQuitarIngActionPerformed

//-----------------------------------------------------CAJAS DE TEXTO---------------------------------------------------------------   

    private void txtIdMenuKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIdMenuKeyTyped
        // TODO add your handling code here:
        char caracter = evt.getKeyChar();
        if(!(((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)))
        {
            String cad=(""+caracter);
            caracter=cad.charAt(0);
            evt.setKeyChar(caracter);
        }else{
            evt.consume();
        }
    }//GEN-LAST:event_txtIdMenuKeyTyped

    private void txtPrecioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioKeyTyped
        // TODO add your handling code here:
        char caracter = evt.getKeyChar();
        if(!(((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)))
        {
            String cad=(""+caracter);
            caracter=cad.charAt(0);
            evt.setKeyChar(caracter);
        }else{
            evt.consume();
        }
    }//GEN-LAST:event_txtPrecioKeyTyped

    private void txtIdIngredienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIdIngredienteKeyTyped
        // TODO add your handling code here:
        char caracter = evt.getKeyChar();
        if(!(((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)))
        {
            String cad=(""+caracter);
            caracter=cad.charAt(0);
            evt.setKeyChar(caracter);
        }else{
            evt.consume();
        }
    }//GEN-LAST:event_txtIdIngredienteKeyTyped

    private void botonNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNuevoActionPerformed
        txtIdMenu.setEnabled(false);
        txtNombre.setEnabled(true);
        txtPrecio.setEnabled(true);
        txtIdIngrediente.setEnabled(false);
        txtIdMenu.setText("");
        txtNombre.setText("");
        txtPrecio.setText("");
        txtIdIngrediente.setText("");
        botonBuscar.setEnabled(false);
        botonModificar.setEnabled(false);
        botonEliminar.setEnabled(false);
        botonNuevo.setEnabled(false);
        botonAgregar.setEnabled(true);
        DefaultTableModel modelo = new DefaultTableModel();
        this.tablaIngredientesMenu.setModel(modelo);
    }//GEN-LAST:event_botonNuevoActionPerformed

    private void txtNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyTyped
        char c=evt.getKeyChar();
        if(Character.isLowerCase(c)){
            String cad=(""+c).toUpperCase();
            c=cad.charAt(0);
            evt.setKeyChar(c);
        }
    }//GEN-LAST:event_txtNombreKeyTyped

    private void botonModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonModificarActionPerformed
        txtIdMenu.setEnabled(false);
        txtNombre.setEnabled(true);
        txtPrecio.setEnabled(true);
        txtIdIngrediente.setEnabled(true);
        botonModificar.setEnabled(false);
        botonBuscar.setEnabled(false);
        botonAgregarIng.setEnabled(true);
        botonQuitarIng.setEnabled(true);
        botonGuardar.setEnabled(true);
        botonNuevo.setEnabled(false);
        botonEliminar.setEnabled(false);
    }//GEN-LAST:event_botonModificarActionPerformed

    private void tablaMenusMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaMenusMouseClicked
        // TODO add your handling code here:
        int nfilas=0;
        int fila=0;
        nfilas=tablaMenus.getSelectedRowCount(); //Obtiene el numero de filas seleccionadas
        if(nfilas==1){
            fila=tablaMenus.getSelectedRow();
            int id=Integer.parseInt((String)tablaMenus.getValueAt(fila, 0).toString());
            txtIdMenu.setText(""+id);
            String nombre=(String)tablaMenus.getValueAt(fila, 1).toString();
            txtNombre.setText(""+nombre);
            Float precio=Float.parseFloat((String)tablaMenus.getValueAt(fila, 2).toString());
            txtPrecio.setText(""+precio);
            botonNuevo.setEnabled(true);
            botonAgregar.setEnabled(false);
            botonModificar.setEnabled(true);
            botonEliminar.setEnabled(true);
            botonGuardar.setEnabled(false);
            botonAgregarIng.setEnabled(false);
            botonQuitarIng.setEnabled(false);
            txtIdMenu.setEnabled(true);
            txtNombre.setEnabled(false);
            txtPrecio.setEnabled(false);
            txtIdIngrediente.setEnabled(false);
            this.cargarTablaIngredientesDelMenu(id);
        }   
    }//GEN-LAST:event_tablaMenusMouseClicked

    private void tablaIngredientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaIngredientesMouseClicked
        // TODO add your handling code here:
        int nfilas=0;
        int fila=0;
        nfilas=tablaIngredientes.getSelectedRowCount(); //Obtiene el numero de filas seleccionadas
        if(nfilas==1){
            fila=tablaIngredientes.getSelectedRow();
            int idIngrediente=Integer.parseInt((String)tablaIngredientes.getValueAt(fila, 0).toString());
            txtIdIngrediente.setText(""+idIngrediente);
        }
    }//GEN-LAST:event_tablaIngredientesMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAgregar;
    private javax.swing.JButton botonAgregarIng;
    private javax.swing.JButton botonBuscar;
    private javax.swing.JButton botonEliminar;
    private javax.swing.JButton botonGuardar;
    private javax.swing.JButton botonModificar;
    private javax.swing.JButton botonNuevo;
    private javax.swing.JButton botonQuitarIng;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable tablaIngredientes;
    private javax.swing.JTable tablaIngredientesMenu;
    private javax.swing.JTable tablaMenus;
    private javax.swing.JTextField txtIdIngrediente;
    private javax.swing.JTextField txtIdMenu;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPrecio;
    // End of variables declaration//GEN-END:variables

    public void cargarTablas(){
        //cargo la tabla de menus
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("IDMENU");//0
        modelo.addColumn("NOMBRE");//1
        modelo.addColumn("PRECIO");//2
        List<Menu> menus = this.controlador.ListarMenus();
        
        modelo.setRowCount(menus.size());
        
        for(int i = 0; i < menus.size(); i++){
            
            modelo.setValueAt(menus.get(i).getIdMenu(), i, 0);
            modelo.setValueAt(menus.get(i).getNombre(), i, 1);
            modelo.setValueAt(menus.get(i).getPrecio(), i, 2);
        }
        this.tablaMenus.setModel(modelo);
        
        //cargo la tabla de ingredientes 
        DefaultTableModel modelo2 = new DefaultTableModel();
        modelo2.addColumn("IDINGREDIENTE");//0
        modelo2.addColumn("NOMBRE");//1
      
        List<Ingrediente> ingredientes = this.controlador.ListarIngredientes();
        
        modelo2.setRowCount(ingredientes.size());
        
        for(int i = 0; i < ingredientes.size(); i++){
            
            modelo2.setValueAt(ingredientes.get(i).getIdIngrediente(), i, 0);
            modelo2.setValueAt(ingredientes.get(i).getNombre(), i, 1);
          
        }
        this.tablaIngredientes.setModel(modelo2);        
    }
    
    
    public void cargarTablaIngredientesDelMenu(int id){
        //cargo la tabla de ingredientes del menu seleccionado
        DefaultTableModel modelo3 = new DefaultTableModel();
        modelo3.addColumn("IDINGREDIENTE");//0
        modelo3.addColumn("NOMBRE");//1
       
        List<Ingrediente> ingredientes = this.controlador.ListarIngredientesDelMenu(id);
        modelo3.setRowCount(ingredientes.size());
        
        for(int i = 0; i < ingredientes.size(); i++){
            
            modelo3.setValueAt(ingredientes.get(i).getIdIngrediente(), i, 0);
            modelo3.setValueAt(ingredientes.get(i).getNombre(), i, 1);
          
        }
        this.tablaIngredientesMenu.setModel(modelo3);
    }

    public void resetear(){
        txtIdMenu.setEnabled(true);
        txtNombre.setEnabled(false);
        txtPrecio.setEnabled(false);
        txtIdIngrediente.setEnabled(false);
        txtIdMenu.setText("");
        txtNombre.setText("");
        txtPrecio.setText("");
        txtIdIngrediente.setText("");
        botonGuardar.setEnabled(false);
        botonModificar.setEnabled(false);
        botonEliminar.setEnabled(false);
        botonBuscar.setEnabled(true);
        botonNuevo.setEnabled(true);
        botonAgregar.setEnabled(false);
        botonAgregarIng.setEnabled(false);
        botonQuitarIng.setEnabled(false);
        cargarTablas();
        DefaultTableModel modelo = new DefaultTableModel();
        this.tablaIngredientesMenu.setModel(modelo);
    }
}
