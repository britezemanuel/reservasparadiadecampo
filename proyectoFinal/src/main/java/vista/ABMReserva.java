package vista;
//-----------------------------------------------------LIBRERIAS---------------------------------------------------------------   
import controlador.Controlador;
import java.awt.Event;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.InputMap;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import modelo.*;

public class ABMReserva extends javax.swing.JPanel {
    private Controlador controlador;
    public ABMReserva() {
        initComponents();
        InputMap map1 = txtId.getInputMap(txtId.WHEN_FOCUSED);
        map1.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
        InputMap map2 = txtTotal.getInputMap(txtTotal.WHEN_FOCUSED);
        map2.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
        InputMap map3 = txtDeposito.getInputMap(txtDeposito.WHEN_FOCUSED);
        map3.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
        InputMap map4 = txtUtensilios.getInputMap(txtUtensilios.WHEN_FOCUSED);
        map4.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
    }

    public ABMReserva(Controlador controlador) {
        this();
        this.controlador = controlador;
        this.cargarTabla();
        this.cargarCombos();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtUtensilios = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtDeposito = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JTextField();
        comboPago = new javax.swing.JComboBox<>();
        chooserFH = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        comboCliente = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        comboMenu = new javax.swing.JComboBox<>();
        botonBuscar = new javax.swing.JButton();
        botonModificar = new javax.swing.JButton();
        botonGuardar = new javax.swing.JButton();
        botonEliminar = new javax.swing.JButton();
        botonNuevo = new javax.swing.JButton();
        botonAgregar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaReservas = new javax.swing.JTable();

        jPanel1.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 968, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 968, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 40, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        jLabel1.setText("ID");

        txtId.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtIdKeyTyped(evt);
            }
        });

        jLabel2.setText("Direccion");

        txtDireccion.setEnabled(false);
        txtDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDireccionKeyTyped(evt);
            }
        });

        jLabel3.setText("Utensilios");

        txtUtensilios.setEnabled(false);
        txtUtensilios.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtUtensiliosKeyTyped(evt);
            }
        });

        jLabel4.setText("Forma de Pago");

        jLabel5.setText("Deposito");

        txtDeposito.setEnabled(false);
        txtDeposito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDepositoKeyTyped(evt);
            }
        });

        jLabel6.setText("Total");

        txtTotal.setEnabled(false);
        txtTotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTotalKeyTyped(evt);
            }
        });

        comboPago.setEnabled(false);

        chooserFH.setDateFormatString("yyyy-MM-dd HH:mm");
        chooserFH.setEnabled(false);

        jLabel7.setText("Fecha y Hora");

        jLabel8.setText("DNI Cliente");

        comboCliente.setEnabled(false);

        jLabel9.setText("Menu");

        comboMenu.setEnabled(false);
        comboMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboMenuActionPerformed(evt);
            }
        });

        botonBuscar.setText("BUSCAR");
        botonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarActionPerformed(evt);
            }
        });

        botonModificar.setText("MODIFICAR");
        botonModificar.setEnabled(false);
        botonModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonModificarActionPerformed(evt);
            }
        });

        botonGuardar.setText("GUARDAR");
        botonGuardar.setEnabled(false);
        botonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarActionPerformed(evt);
            }
        });

        botonEliminar.setText("ELIMINAR");
        botonEliminar.setEnabled(false);
        botonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarActionPerformed(evt);
            }
        });

        botonNuevo.setText("NUEVO");
        botonNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNuevoActionPerformed(evt);
            }
        });

        botonAgregar.setText("AGREGAR");
        botonAgregar.setEnabled(false);
        botonAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarActionPerformed(evt);
            }
        });

        tablaReservas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "IDRESERVA", "FECHA Y HORA", "CLIENTE", "MENU", "DIRECCION", "UTENSILIOS", "DEPOSITO", "TOTAL"
            }
        ));
        tablaReservas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaReservasMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaReservas);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtUtensilios, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(63, 63, 63)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(chooserFH, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(comboCliente, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(comboMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(63, 63, 63)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboPago, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDeposito, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(39, 39, 39)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(botonAgregar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonModificar, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(botonGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 128, Short.MAX_VALUE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(botonBuscar)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(botonModificar)
                            .addComponent(comboPago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(botonGuardar)
                        .addGap(18, 18, 18)
                        .addComponent(botonEliminar)
                        .addGap(18, 18, 18)
                        .addComponent(botonNuevo))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel7)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chooserFH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel8)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDeposito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel9)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtUtensilios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(botonAgregar)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(119, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

//-----------------------------------------------------BOTONES---------------------------------------------------------------   

    private void botonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarActionPerformed
        if("".equals(txtDireccion.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO DIRECCION VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            if("".equals(txtUtensilios.getText().replaceAll(" ", ""))){
                JOptionPane.showMessageDialog(this, "CAMPO UTENSILIOS VACIO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                if(chooserFH.getDate()==null){
                    JOptionPane.showMessageDialog(this, "CALENDARIO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                }else{
                    if(comboCliente.getSelectedItem()==""){
                        JOptionPane.showMessageDialog(this, "CAMPO CLIENTE VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                    }else{
                        if(comboMenu.getSelectedItem()==""){
                            JOptionPane.showMessageDialog(this, "CAMPO MENU VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                        }else{
                            if(comboPago.getSelectedItem()==""){
                                JOptionPane.showMessageDialog(this, "CAMPO FORMA DE PAGO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                            }else{
                                if("".equals(txtDeposito.getText().replaceAll(" ", ""))){
                                    JOptionPane.showMessageDialog(this, "CAMPO DEPOSITO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                                }else{
                                    if("".equals(txtTotal.getText().replaceAll(" ", ""))){
                                        JOptionPane.showMessageDialog(this, "CAMPO TOTAL VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                                    }else{
                                        float deposito = Float.parseFloat(txtDeposito.getText());
                                        if(deposito<=0){
                                            JOptionPane.showMessageDialog(this, "DEBE INGRESAR UN VALOR SUPERIOR A 0", "Error", JOptionPane.ERROR_MESSAGE);
                                        }else{
                                            float total = Float.parseFloat(txtTotal.getText());
                                            if(deposito>total){
                                                //ERROR EL DEPOSITO ES MAYOR AL TOTAL
                                                JOptionPane.showMessageDialog(this, "EL VALOR INGRESADO ES SUPERIOR AL SOLICITADO", "Error", JOptionPane.ERROR_MESSAGE);
                                            }else{
                                                //AQUI EMPIEZA EL CODIGO
                                                String direccion = txtDireccion.getText();
                                                Date horaFecha = chooserFH.getDate();
                                                int utensilios = Integer.parseInt(txtUtensilios.getText());
                                                String formaPago = (String) comboPago.getSelectedItem();
                                                String dniCliente = (String) comboCliente.getSelectedItem();
                                                Cliente cli = this.controlador.buscarCliente(dniCliente);
                                                String nombreMenu = (String) comboMenu.getSelectedItem();
                                                Menu menu = this.controlador.buscarMenu(nombreMenu);
                                                Reserva res = new Reserva(horaFecha, direccion, utensilios , deposito, formaPago, "VISIBLE",  cli, menu);
                                                this.controlador.persistirReserva(res);
                                                JOptionPane.showMessageDialog(this, "RESERVA CARGADA EXITOSAMENTE", "Aviso", INFORMATION_MESSAGE);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        resetear();
    }//GEN-LAST:event_botonAgregarActionPerformed

    private void botonNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNuevoActionPerformed
        chooserFH.setEnabled(true);
        txtId.setEnabled(false);
        txtDireccion.setEnabled(true);
        txtUtensilios.setEnabled(true);
        txtDeposito.setEnabled(true);
        comboPago.setEnabled(true);
        comboCliente.setEnabled(true);
        comboMenu.setEnabled(true);
        txtId.setText("");
        txtDireccion.setText("");
        txtUtensilios.setText("");
        txtDeposito.setText("");
        txtTotal.setText("");
        chooserFH.setDate(null);
        comboCliente.setSelectedIndex(0);
        comboPago.setSelectedIndex(0);
        comboMenu.setSelectedIndex(0);
        botonAgregar.setEnabled(true);
        botonNuevo.setEnabled(false);
        botonModificar.setEnabled(false);
        botonEliminar.setEnabled(false);
        botonBuscar.setEnabled(false);
        botonAgregar.setEnabled(true);
    }//GEN-LAST:event_botonNuevoActionPerformed

    private void botonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarActionPerformed
        // TODO add your handling code here:
        if("".equals(txtId.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO ID VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            int id = Integer.parseInt(txtId.getText());
            Reserva ing = this.controlador.buscarReserva(id);
            if (ing == null){
                JOptionPane.showMessageDialog(this, "ID NO ENCONTRADO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                this.controlador.eliminarReserva(ing);
                JOptionPane.showMessageDialog(this, "RESERVA ELIMINADA EXITOSAMENTE", "Aviso", INFORMATION_MESSAGE);
                this.cargarTabla();
            }
        }
        resetear();
    }//GEN-LAST:event_botonEliminarActionPerformed

    private void botonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarActionPerformed
        // TODO add your handling code here:
        if("".equals(txtDireccion.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO DIRECCION VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            if("".equals(txtUtensilios.getText().replaceAll(" ", ""))){
                JOptionPane.showMessageDialog(this, "CAMPO UTENSILIOS VACIO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                if(chooserFH.getDate()==null){
                    JOptionPane.showMessageDialog(this, "CAMPO CALENDARIO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                }else{
                    if(comboCliente.getSelectedItem()==""){
                        JOptionPane.showMessageDialog(this, "CAMPO CLIENTE VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                    }else{
                        if(comboMenu.getSelectedItem()==""){
                            JOptionPane.showMessageDialog(this, "CAMPO MENU VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                        }else{
                            if(comboPago.getSelectedItem()==""){
                                JOptionPane.showMessageDialog(this, "CAMPO FORMA DE PAGO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                            }else{
                                if("".equals(txtDeposito.getText().replaceAll(" ", ""))){
                                    JOptionPane.showMessageDialog(this, "CAMPO DEPOSITO VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                                }else{
                                    if("".equals(txtTotal.getText().replaceAll(" ", ""))){
                                        JOptionPane.showMessageDialog(this, "CAMPO TOTAL VACIO", "Error", JOptionPane.ERROR_MESSAGE);
                                    }else{
                                        float deposito = Float.parseFloat(txtDeposito.getText());
                                        if(deposito<=0){
                                            JOptionPane.showMessageDialog(this, "DEBE INGRESAR UN VALOR SUPERIOR A 0", "Error", JOptionPane.ERROR_MESSAGE);
                                        }else{
                                            float total = Float.parseFloat(txtTotal.getText());
                                            if(deposito>total){
                                                //ERROR EL DEPOSITO ES MAYOR AL TOTAL
                                                JOptionPane.showMessageDialog(this, "EL VALOR INGRESADO ES SUPERIOR AL SOLICITADO", "Error", JOptionPane.ERROR_MESSAGE);
                                            }else{
                                                int id = Integer.parseInt(txtId.getText());
                                                Reserva res = this.controlador.buscarReserva(id);
                                                if (res != null){
                                                    String direccion = txtDireccion.getText();
                                                    Date horaFecha = chooserFH.getDate();
                                                    total = Float.parseFloat(txtTotal.getText());
                                                    deposito = Float.parseFloat(txtDeposito.getText());
                                                    int utensilios = Integer.parseInt(txtUtensilios.getText());
                                                    String formaPago = (String) comboPago.getSelectedItem();
                                                    String dniCliente = (String) comboCliente.getSelectedItem();
                                                    Cliente cli = this.controlador.buscarCliente(dniCliente);
                                                    String nombreMenu = (String) comboMenu.getSelectedItem();
                                                    Menu menu = this.controlador.buscarMenu(nombreMenu);
                                                    res.setCliente(cli);
                                                    res.setDeposito(deposito);
                                                    res.setDireccion(direccion);
                                                    res.setFormaDePago(formaPago);
                                                    res.setHorafecha(horaFecha);
                                                    res.setMenu(menu);
                                                    res.setTotal(total);
                                                    res.setUtensilios(utensilios);
                                                    this.controlador.actualizarReserva(res);
                                                    JOptionPane.showMessageDialog(this, "RESERVA ACTUALIZADA EXITOSAMENTE", "Aviso", INFORMATION_MESSAGE);
                                                    this.cargarTabla();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        resetear();
    }//GEN-LAST:event_botonGuardarActionPerformed

    private void botonModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonModificarActionPerformed
        // TODO add your handling code here:
        txtId.setEnabled(false);
        txtDireccion.setEnabled(true);
        txtUtensilios.setEnabled(true);
        txtDeposito.setEnabled(true);
        txtTotal.setEnabled(true);
        comboPago.setEnabled(true);
        comboCliente.setEnabled(true);
        comboMenu.setEnabled(true);
        chooserFH.setEnabled(true);
        botonBuscar.setEnabled(false);
        botonModificar.setEnabled(false);
        botonGuardar.setEnabled(true);
        botonEliminar.setEnabled(false);
        botonNuevo.setEnabled(false);
    }//GEN-LAST:event_botonModificarActionPerformed

    private void botonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarActionPerformed
        // TODO add your handling code here:
        if("".equals(txtId.getText().replaceAll(" ", ""))){
            JOptionPane.showMessageDialog(this, "CAMPO ID VACIO", "Error", JOptionPane.ERROR_MESSAGE);
        }else{
            int id = Integer.parseInt(txtId.getText());
            Reserva res = this.controlador.buscarReserva(id);
            if(res == null){
                JOptionPane.showMessageDialog(this, "ID NO ENCONTRADO", "Error", JOptionPane.ERROR_MESSAGE);
            }else{
                txtDireccion.setText(res.getDireccion());
                txtUtensilios.setText(String.valueOf(res.getUtensilios()));
                txtDeposito.setText(String.valueOf(res.getDeposito()));
                txtTotal.setText(String.valueOf(res.getTotal()));
                comboPago.setSelectedItem(res.getFormaDePago());
                comboCliente.setSelectedItem(res.getCliente().getDocumento());
                comboMenu.setSelectedItem(res.getMenu().getNombre());
                chooserFH.setDate(res.getHorafecha());
                botonModificar.setEnabled(true);
                botonEliminar.setEnabled(true);
            }
        }
    }//GEN-LAST:event_botonBuscarActionPerformed

//-----------------------------------------------------CAMPOS DE TEXTOS---------------------------------------------------------------   

    private void comboMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboMenuActionPerformed
        if(comboMenu.getSelectedItem()!=""){
            String nombreMenu = (String) comboMenu.getSelectedItem();
            Menu menu = this.controlador.buscarMenu(nombreMenu);
            txtTotal.setText(String.valueOf(menu.getPrecio()));
        }else{
            txtTotal.setText("");
        }
    }//GEN-LAST:event_comboMenuActionPerformed

    private void txtDepositoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDepositoKeyTyped
        // TODO add your handling code here:
        char caracter = evt.getKeyChar();
        if(!(((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)))
        {
            String cad=(""+caracter);
            caracter=cad.charAt(0);
            evt.setKeyChar(caracter);
        }else{
            evt.consume();
        }
    }//GEN-LAST:event_txtDepositoKeyTyped

    private void txtUtensiliosKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUtensiliosKeyTyped
        // TODO add your handling code here:
        char caracter = evt.getKeyChar();
        if(!(((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)))
        {
            String cad=(""+caracter);
            caracter=cad.charAt(0);
            evt.setKeyChar(caracter);
        }else{
            evt.consume();
        }
    }//GEN-LAST:event_txtUtensiliosKeyTyped

    private void txtDireccionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDireccionKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
        if(Character.isLowerCase(c)){
            String cad=(""+c).toUpperCase();
            c=cad.charAt(0);
            evt.setKeyChar(c);
        }
    }//GEN-LAST:event_txtDireccionKeyTyped

    private void txtIdKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIdKeyTyped
        // TODO add your handling code here:
        char caracter = evt.getKeyChar();
        if(!(((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)))
        {
            String cad=(""+caracter);
            caracter=cad.charAt(0);
            evt.setKeyChar(caracter);
        }else{
            evt.consume();
        }
    }//GEN-LAST:event_txtIdKeyTyped

    private void txtTotalKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTotalKeyTyped
        // TODO add your handling code here:
        char caracter = evt.getKeyChar();
        if(!(((caracter < '0') || (caracter > '9')) && (caracter != '\b' /*corresponde a BACK_SPACE*/)))
        {
            String cad=(""+caracter);
            caracter=cad.charAt(0);
            evt.setKeyChar(caracter);
        }else{
            evt.consume();
        }
    }//GEN-LAST:event_txtTotalKeyTyped

    private void tablaReservasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaReservasMouseClicked
        // TODO add your handling code here:
        int nfilas=0;
        int fila=0;
        nfilas=tablaReservas.getSelectedRowCount(); //Obtiene el numero de filas seleccionadas
        if(nfilas==1){
            fila=tablaReservas.getSelectedRow();
            int id=Integer.parseInt((String)tablaReservas.getValueAt(fila, 0).toString());
            Reserva res = this.controlador.buscarReserva(id);
            txtId.setText(""+id);
            txtDireccion.setText(res.getDireccion());
            txtUtensilios.setText(String.valueOf(res.getUtensilios()));
            txtDeposito.setText(String.valueOf(res.getDeposito()));
            txtTotal.setText(String.valueOf(res.getTotal()));
            comboPago.setSelectedItem(res.getFormaDePago());
            comboCliente.setSelectedItem(res.getCliente().getDocumento());
            comboMenu.setSelectedItem(res.getMenu().getNombre());
            chooserFH.setDate(res.getHorafecha());
            botonModificar.setEnabled(true);
            botonEliminar.setEnabled(true);
            botonNuevo.setEnabled(true);
            botonGuardar.setEnabled(false);
            chooserFH.setEnabled(false);
            txtId.setEnabled(true);
            txtDireccion.setEnabled(false);
            txtUtensilios.setEnabled(false);
            txtDeposito.setEnabled(false);
            txtTotal.setEnabled(false);
            comboCliente.setEnabled(false);
            comboMenu.setEnabled(false);
            comboPago.setEnabled(false);
            txtDireccion.setEnabled(false);
        }
    }//GEN-LAST:event_tablaReservasMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAgregar;
    private javax.swing.JButton botonBuscar;
    private javax.swing.JButton botonEliminar;
    private javax.swing.JButton botonGuardar;
    private javax.swing.JButton botonModificar;
    private javax.swing.JButton botonNuevo;
    private com.toedter.calendar.JDateChooser chooserFH;
    private javax.swing.JComboBox<String> comboCliente;
    private javax.swing.JComboBox<String> comboMenu;
    private javax.swing.JComboBox<String> comboPago;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaReservas;
    private javax.swing.JTextField txtDeposito;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtTotal;
    private javax.swing.JTextField txtUtensilios;
    // End of variables declaration//GEN-END:variables

//-----------------------------------------------------FUNCIONES---------------------------------------------------------------   

    public void cargarCombos(){
        // cargo combo forma de pago
            comboPago.addItem("");
            comboPago.addItem("EFECTIVO");
            comboPago.addItem("CREDITO");
            comboPago.addItem("DEBITO");
        
        //cargo combo de clientes
            List <Cliente> listaClientes= controlador.ListarClientes();
            comboCliente.addItem("");
            for(int i = 0; i < listaClientes.size() ; i++ ){
                 comboCliente.addItem(listaClientes.get(i).getDocumento());
            }
        //cargo combo de menus
            List <Menu> listaMenus= controlador.ListarMenus();
            comboMenu.addItem("");
            for(int i = 0; i < listaMenus.size() ; i++ ){
                 comboMenu.addItem(listaMenus.get(i).getNombre());
            }
   }
    
       public void cargarTabla(){
        //cargo la tabla de reservas
        String texto="";
        String fecha="";
        DefaultTableModel modelo = new DefaultTableModel();
        modelo.addColumn("IDRESERVA");//0
        modelo.addColumn("FECHA Y HORA");//1
        modelo.addColumn("CLIENTE");//2
        modelo.addColumn("DIRECCION");//3
        modelo.addColumn("MENU");//4
        modelo.addColumn("UTENCILIOS");//5
        modelo.addColumn("DEPOSITO");//6
        modelo.addColumn("TOTAL");//7
       
        
        List<Reserva> reservas = this.controlador.ListarReservas();
        
        modelo.setRowCount(reservas.size());
        
        for(int i = 0; i < reservas.size(); i++){
            
            modelo.setValueAt(reservas.get(i).getIdReserva(), i, 0);
            SimpleDateFormat formato= new SimpleDateFormat("yyyy-MM-dd HH:mm");
            fecha= formato.format(reservas.get(i).getHorafecha());
            modelo.setValueAt(fecha, i, 1);
            texto=reservas.get(i).getCliente().getApellido() +" "+ reservas.get(i).getCliente().getNombre();
            modelo.setValueAt(texto, i, 2);
            modelo.setValueAt(reservas.get(i).getDireccion(), i, 3);
            texto= reservas.get(i).getMenu().getNombre();
            modelo.setValueAt(texto, i, 4);
            modelo.setValueAt(reservas.get(i).getUtensilios(), i, 5);
            modelo.setValueAt(reservas.get(i).getDeposito(), i, 6);
            modelo.setValueAt(reservas.get(i).getTotal(), i, 7);
        }
        this.tablaReservas.setModel(modelo);

    }
    
    public void resetear(){
        chooserFH.setEnabled(false);
        txtId.setEnabled(true);
        txtDireccion.setEnabled(false);
        txtUtensilios.setEnabled(false);
        txtDeposito.setEnabled(false);
        txtTotal.setEnabled(false);
        comboCliente.setEnabled(false);
        comboMenu.setEnabled(false);
        comboPago.setEnabled(false);
        txtDireccion.setEnabled(false);
        chooserFH.setDate(null);
        txtId.setText("");
        txtDireccion.setText("");
        txtUtensilios.setText("");
        txtDeposito.setText("");
        txtTotal.setText("");
        comboCliente.setSelectedIndex(0);
        comboMenu.setSelectedIndex(0);
        comboPago.setSelectedIndex(0);
        botonBuscar.setEnabled(true);
        botonModificar.setEnabled(false);
        botonGuardar.setEnabled(false);
        botonEliminar.setEnabled(false);
        botonNuevo.setEnabled(true);
        botonAgregar.setEnabled(false);
        this.cargarTabla();
    }
}