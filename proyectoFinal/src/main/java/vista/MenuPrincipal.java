package vista;

import controlador.Controlador;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class MenuPrincipal extends javax.swing.JFrame {
    private Controlador controlador;
    
    /** Creates new form MenuPrincipal */
    public MenuPrincipal() {
        initComponents();
    }

    public MenuPrincipal(Controlador controlador) {
        this();
        this.controlador = controlador;
    }
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuItem1 = new javax.swing.JMenuItem();
        panelActual = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        Cliente = new javax.swing.JMenu();
        ABMCliente = new javax.swing.JMenuItem();
        Reserva = new javax.swing.JMenu();
        ABMReserva = new javax.swing.JMenuItem();
        Menu = new javax.swing.JMenu();
        ABMMenuItem = new javax.swing.JMenuItem();
        Ingrediente = new javax.swing.JMenu();
        ABMIngredienteItem = new javax.swing.JMenuItem();

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelActual.setLayout(new java.awt.BorderLayout());

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/META-INF/bienvenida.png"))); // NOI18N
        panelActual.add(jLabel1, java.awt.BorderLayout.CENTER);

        Cliente.setText("Cliente");

        ABMCliente.setText("ABMCliente");
        ABMCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ABMClienteActionPerformed(evt);
            }
        });
        Cliente.add(ABMCliente);

        jMenuBar1.add(Cliente);

        Reserva.setText("Reserva");

        ABMReserva.setText("ABMReserva");
        ABMReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ABMReservaActionPerformed(evt);
            }
        });
        Reserva.add(ABMReserva);

        jMenuBar1.add(Reserva);

        Menu.setText("Menu");

        ABMMenuItem.setText("ABMMenu");
        ABMMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ABMMenuItemActionPerformed(evt);
            }
        });
        Menu.add(ABMMenuItem);

        jMenuBar1.add(Menu);

        Ingrediente.setText("Ingrediente");

        ABMIngredienteItem.setText("ABMIngrediente");
        ABMIngredienteItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ABMIngredienteItemActionPerformed(evt);
            }
        });
        Ingrediente.add(ABMIngredienteItem);

        jMenuBar1.add(Ingrediente);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelActual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelActual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ABMClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ABMClienteActionPerformed
        // TODO add your handling code here:
        ABMCliente cli = new ABMCliente(controlador);
        cambiarPanel(cli);
    }//GEN-LAST:event_ABMClienteActionPerformed

    private void ABMReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ABMReservaActionPerformed
        // TODO add your handling code here:
        ABMReserva res = new ABMReserva(controlador);
        cambiarPanel(res);
    }//GEN-LAST:event_ABMReservaActionPerformed

    private void ABMIngredienteItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ABMIngredienteItemActionPerformed
        ABMIngrediente ing = new ABMIngrediente(controlador);
        cambiarPanel(ing);
    }//GEN-LAST:event_ABMIngredienteItemActionPerformed

    private void ABMMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ABMMenuItemActionPerformed
        ABMMenu men = new ABMMenu(controlador);
        cambiarPanel(men);
    }//GEN-LAST:event_ABMMenuItemActionPerformed


    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem ABMCliente;
    private javax.swing.JMenuItem ABMIngredienteItem;
    private javax.swing.JMenuItem ABMMenuItem;
    private javax.swing.JMenuItem ABMReserva;
    private javax.swing.JMenu Cliente;
    private javax.swing.JMenu Ingrediente;
    private javax.swing.JMenu Menu;
    private javax.swing.JMenu Reserva;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel panelActual;
    // End of variables declaration//GEN-END:variables

    private void cambiarPanel(JPanel panel) {
        panelActual.removeAll();
        panelActual.add(panel,BorderLayout.CENTER);
        panelActual.revalidate();
        panelActual.paintAll(getGraphics());
    }
}
