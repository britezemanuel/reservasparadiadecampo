package persistencia;
//-----------------------------------------------------LIBRERIAS---------------------------------------------------------------   
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import modelo.*;

public class ControladorPersistencia {
    private final EntityManagerFactory miFabricaDeEntidades;
    private final EntityManager em;
    
//-------------------------------------------------PERSISTENCIAS-------------------------------------------------    
    public ControladorPersistencia() {
        miFabricaDeEntidades = Persistence.createEntityManagerFactory("unidadPersistencia1");
        em = miFabricaDeEntidades.createEntityManager();
    }
    public void persistirCliente(Cliente unCliente){
        this.em.getTransaction().begin();
        this.em.persist(unCliente);
        this.em.getTransaction().commit();
    }
    public void persistirMenu(Menu unMenu){
        this.em.getTransaction().begin();
        this.em.persist(unMenu);
        this.em.getTransaction().commit();
    }
    public void persistirIngrediente(Ingrediente unIngrediente){
        this.em.getTransaction().begin();
        this.em.persist(unIngrediente);
        this.em.getTransaction().commit();
    }
    public void persistirReserva(Reserva unaReserva){
        this.em.getTransaction().begin();
        this.em.persist(unaReserva);
        this.em.getTransaction().commit();
    }
    
//-----------------------------------------------------ACTUALIZAR-----------------------------------------------------------------------------
    public void actualizarCliente(Cliente unCliente){
        this.em.getTransaction().begin();
        this.em.merge(unCliente);
        this.em.getTransaction().commit();
    }
    public void actualizarIngrediente(Ingrediente unIngrediente){
        this.em.getTransaction().begin();
        this.em.merge(unIngrediente);
        this.em.getTransaction().commit();
    }
    public void actualizarMenu(Menu unMenu){
        this.em.getTransaction().begin();
        this.em.merge(unMenu);
        this.em.getTransaction().commit();
    }
    public void actualizarReserva(Reserva unaReserva){
        this.em.getTransaction().begin();
        this.em.merge(unaReserva);
        this.em.getTransaction().commit();
    }
    
//-----------------------------------------------------ELIMINAR---------------------------------------------------------------
    public void eliminarCliente(Cliente unCliente){
        String estado = "OCULTO";
        unCliente.setVisible(estado);           
        this.em.getTransaction().begin();
        this.em.merge(unCliente);
        this.em.getTransaction().commit();
    }
    public void eliminarIngrediente(Ingrediente unIngrediente){
        String estado = "OCULTO";
        unIngrediente.setVisible(estado);           
        this.em.getTransaction().begin();
        this.em.merge(unIngrediente);
        this.em.getTransaction().commit();
    }
    public void eliminarMenu(Menu unMenu){
        String estado = "OCULTO";
        unMenu.setVisible(estado);           
        this.em.getTransaction().begin();
        this.em.merge(unMenu);
        this.em.getTransaction().commit();
    }
    public void eliminarReserva(Reserva unaReserva){
        String estado = "OCULTO";
        unaReserva.setVisible(estado);           
        this.em.getTransaction().begin();
        this.em.merge(unaReserva);
        this.em.getTransaction().commit();
    }
    
    
//-----------------------------------------------------LISTAR---------------------------------------------------------------

    public List<Cliente> ListarClientes(){        
        Query query = em.createQuery("Select c FROM Cliente c WHERE c.visible = 'VISIBLE' ORDER BY c.documento ASC");        
        List<Cliente> clientes= query.getResultList();      
        return clientes;
    }
    public List<Cliente> ListarClientesAlfabeticamente(){        
        Query query = em.createQuery("Select c FROM Cliente c WHERE c.visible = 'VISIBLE' ORDER BY c.apellido ASC");        
        List<Cliente> clientes= query.getResultList();      
        return clientes;
    }
    public List<Ingrediente> ListarIngredientes(){        
        Query query = em.createQuery("Select i FROM Ingrediente i WHERE i.visible = 'VISIBLE' ORDER BY i.nombre ASC");        
        List<Ingrediente> ingredientes= query.getResultList();         
        return ingredientes;        
    }     
    public List<Menu> ListarMenus(){        
        Query query = em.createQuery("Select m FROM Menu m WHERE m.visible = 'VISIBLE' ORDER BY m.nombre ASC");        
        List<Menu> menus= query.getResultList();         
        return menus;
    }      
    public List<Reserva> ListarReservas(){        
        Query query = em.createQuery("Select r FROM Reserva r WHERE r.visible = 'VISIBLE' ORDER BY r.horafecha ASC");        
        List<Reserva> reservas= query.getResultList();       
        return reservas;
    } 
    public List<Ingrediente> ListarIngredientesDelMenu(int id){
        Query query = this.em.createQuery("Select i FROM Ingrediente i JOIN i.menus m WHERE m.idMenu = :id AND i.visible='VISIBLE' ORDER BY i.nombre ASC");
        query.setParameter("id", id);
        List<Ingrediente> ingredientes = query.getResultList();
        return ingredientes;
    }
    
//-----------------------------------------------------BUSCAR---------------------------------------------------------------

    public Cliente BuscarCliente(int id) {
        Cliente cli= null;
	//EntityManager em= tjc.getEntityManager();/	
        Query query = this.em.createQuery("SELECT c FROM Cliente c WHERE c.idCliente =:id AND c.visible='VISIBLE'");
        query.setParameter ("id", id);
        List<Cliente> clientes = query.getResultList();
        if(!clientes.isEmpty()){
         cli = (Cliente) query.getSingleResult();
        }                           
        return cli;
    }    
    public Cliente BuscarCliente(String dni) {
        Cliente cli= null;
	//EntityManager em= tjc.getEntityManager();
	Query query = this.em.createQuery("SELECT c FROM Cliente c WHERE c.documento =:dni AND c.visible = 'VISIBLE'");
        query.setParameter ("dni", dni);
        List<Cliente> clientes = query.getResultList();
        if(!clientes.isEmpty()){
            cli = (Cliente) query.getSingleResult();
        }
	return cli;
    }          
    public Ingrediente BuscarIngrediente (int id){
        Ingrediente ingre = null;
            Query query = this.em.createQuery("SELECT i FROM Ingrediente i WHERE i.idIngrediente =:id AND i.visible='VISIBLE'");
            query.setParameter ("id", id);
            List<Ingrediente> ingredientes = query.getResultList();
            if(!ingredientes.isEmpty()){
                ingre = (Ingrediente) query.getSingleResult();
            }
        return ingre;
    }     
    public Ingrediente BuscarIngrediente (String nombre){
        Ingrediente ingre = null;
        Query query = this.em.createQuery("SELECT i FROM Ingrediente i WHERE i.nombre =:nombre AND i.visible='VISIBLE'");
        query.setParameter ("nombre", nombre);
        List<Ingrediente> ingredientes = query.getResultList();
        if(!ingredientes.isEmpty()){
            ingre = (Ingrediente) query.getSingleResult();
        }               
        return ingre;
    }       
    public Menu BuscarMenu (int id){
        Menu menu = null;
        Query query = this.em.createQuery("SELECT m FROM Menu m WHERE m.idMenu =:id AND m.visible='VISIBLE'");
        query.setParameter ("id", id);
        List<Menu> menus = query.getResultList();
        if(!menus.isEmpty()){
            menu = (Menu) query.getSingleResult();
        }               
        return menu;
    }        
    public Menu BuscarMenu (String nombre){
        Menu menu = null;
        Query query = this.em.createQuery("SELECT m FROM Menu m WHERE m.nombre =:nombre AND m.visible='VISIBLE'");
        query.setParameter ("nombre", nombre);
        List<Menu> menus = query.getResultList();
        if(!menus.isEmpty()){
            menu = (Menu) query.getSingleResult();
        }               
        return menu;
    }        
    public Reserva BuscarReserva (int id){
        Reserva reserva = null;
            Query query = this.em.createQuery("SELECT r FROM Reserva r WHERE r.idReserva =:id AND r.visible='VISIBLE'");
            query.setParameter ("id", id);
            List<Reserva> reservas = query.getResultList();
            if(!reservas.isEmpty()){
                reserva = (Reserva) query.getSingleResult();
            }               
        return reserva;
    }
}
