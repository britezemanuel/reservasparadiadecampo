package modelo;
//-----------------------------------------------------LIBRERIAS---------------------------------------------------------------   
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
//-----------------------------------------------------TABLA Y RELACIONES---------------------------------------------------------------   
@Entity
@Table (name="ingrediente")
public class Ingrediente {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idIngrediente;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "visible")
    private String visible;
    
    @ManyToMany (mappedBy = "ingredientes" , cascade= CascadeType.REMOVE)
    private List<Menu>menus;
        
//-----------------------------------------------------CONSTRUCTOR---------------------------------------------------------------    
    public Ingrediente() {
    }

    public Ingrediente(String nombre, String visible) {
        this.nombre = nombre;
        this.visible = visible;
    }
    
//-----------------------------------------------------GET---------------------------------------------------------------
    public int getIdIngrediente() {
        return idIngrediente;
    }    
    public String getNombre() {
        return nombre;
    }
    public String getVisible() {
        return visible;
    }
    
//-----------------------------------------------------SET---------------------------------------------------------------
    public void setIdIngrediente(int idIngrediente) {
        this.idIngrediente = idIngrediente;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setVisible(String visible) {
        this.visible = visible;
    }
    public List<Menu> getMenus() {
        return menus;
    }
    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }    
    public void agregarMenu(Menu menu){
        this.menus.add(menu);
    }    
    public void quitarMenu (Menu menu){
        int cont=0;
        for (Menu men : this.menus){
            if (men.getIdMenu() == menu.getIdMenu()){
                break;
            }
            cont++;
        }
        menus.remove(cont);
    }
}
