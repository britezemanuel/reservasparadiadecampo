package modelo;
//-----------------------------------------------------LIBRERIAS---------------------------------------------------------------   
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
//-----------------------------------------------------TABLA Y RELACIONES---------------------------------------------------------------   
@Entity
@Table (name="menu")//las tablas van en plural
public class Menu {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    int idMenu;
    
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "visible")
    private String visible;
    
    @Column(name = "precio")
    float precio;

    @ManyToMany(cascade = CascadeType.REMOVE)
    private List<Ingrediente> ingredientes;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "menu")
    private List<Reserva>reservas;
    
//-----------------------------------------------------CONSTRUCTOR---------------------------------------------------------------
    public Menu() {
        this.ingredientes=new ArrayList();
    }
    public Menu(String nombre, float precio, String visible, List<Ingrediente> ingredientes) {
        this.nombre = nombre;
        this.precio = precio;
        this.visible = visible;
        this.ingredientes = ingredientes;
    }
    public Menu(String nombre, float precio, String visible) {
        this.nombre = nombre;
        this.precio = precio;
        this.visible = visible;
        this.ingredientes=new ArrayList();
    } 
    
//-----------------------------------------------------METODOS---------------------------------------------------------------

    public void agregarIngrediente(Ingrediente ing){
        this.ingredientes.add(ing);
    }    
    public void quitarIngrediente(Ingrediente ing){
        int cont=0;       
        for (Ingrediente ingre: this.ingredientes){        
            if(ingre.getIdIngrediente() == ing.getIdIngrediente()){
                break;
            }
            cont++;
        }
        ingredientes.remove(cont);        
    } 
    public int comprobarIngrediente(Ingrediente ing){//
        /*
        * Busca si ya existe registrado el requisito para el tramite
        * Si existe retorna 1
        * Caso contrario retorna 0; 
        */
        int ban = 0;
        if (this.ingredientes.isEmpty()){
            ban=0;
        }else{
            for (Ingrediente ingre: this.ingredientes) {
                if (ing.getIdIngrediente() == ingre.getIdIngrediente() ) {
                       ban = 1;//SIGNIFICA QUE SE ENCONTRÓ EL INGREDIENTE
                       break;
                }
            }
        }	
        return ban;
    }
    
//-----------------------------------------------------GET---------------------------------------------------------------
    public int getIdMenu() {
        return idMenu;
    }
    public String getNombre() {
        return nombre;
    }
    public float getPrecio() {
        return precio;
    }
    public String getVisible() {
        return visible;
    }
    public List<Ingrediente> getIngredientes() {
        return ingredientes;
    }
    public List<Reserva> getReservas() {
        return reservas;
    }
//-----------------------------------------------------SET---------------------------------------------------------------
    public void setIdMenu(int idMenu) {
        this.idMenu = idMenu;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setPrecio(float precio) {
        this.precio = precio;
    }
    public void setVisible(String visible) {
        this.visible = visible;
    }
    public void setIngredientes(List<Ingrediente> ingredientes) {
        this.ingredientes = ingredientes;
    }
    public void setReservas(List<Reserva> reservas) {
        this.reservas = reservas;
    }         
}
