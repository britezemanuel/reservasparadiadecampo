package modelo;

//-----------------------------------------------------LIBRERIAS---------------------------------------------------------------    
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//-----------------------------------------------------TABLA Y RELACIONES---------------------------------------------------------------    
@Entity
@Table (name="cliente")
public class Cliente {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    int idCliente;
    @Column(name = "apellido")
    private String apellido;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "documento")
    private String documento;
    @Column(name = "domicilio")
    private String domicilio;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "visible")
    private String visible;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cliente")
    private List<Reserva>reservas;
    
//-----------------------------------------------------CONSTRUCTOR---------------------------------------------------------------
    public Cliente() {
    }

    public Cliente(String apellido, String nombre, String documento, String domicilio, String telefono, String visible) {
        this.apellido = apellido;
        this.nombre = nombre;
        this.documento = documento;
        this.domicilio = domicilio;
        this.telefono = telefono;
        this.visible = visible;
    }
    
    
//-----------------------------------------------------GET---------------------------------------------------------------
    public int getIdCliente() {
        return idCliente;
    }
    public String getApellido() {
        return apellido;
    }
    public String getNombre() {
        return nombre;
    }
    public String getDocumento() {
        return documento;
    }
    public String getDomicilio() {
        return domicilio;
    }
    public String getTelefono() {
        return telefono;
    }
    public String getVisible() {
        return visible;
    }
     public List<Reserva> getReservas() {
        return reservas;
    }
    
//-----------------------------------------------------SET---------------------------------------------------------------
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    public void setDocumento(String documento) {
        this.documento = documento;
    }
    public void setVisible(String visible) {
        this.visible = visible;
    }
    public void setReservas(List<Reserva> reservas) {
        this.reservas = reservas;
    }
}
