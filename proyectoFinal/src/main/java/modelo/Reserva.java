package modelo;
//-----------------------------------------------------LIBRERIAS---------------------------------------------------------------   
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
//-----------------------------------------------------TABLA Y RELACIONES---------------------------------------------------------------   
@Entity
@Table (name="reserva")
public class Reserva {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idReserva;
    
    @Column(name = "horafecha")
    private Date horafecha;
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "utensilios")
    private int utensilios;
    @Column(name = "deposito")
    private float deposito;
    @Column(name = "formaDePago")
    private  String formaDePago;
    @Column(name = "total")
    private float total;
    @Column(name = "visible")
    private String visible;
    
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_CLIENTE")
    private Cliente cliente;
    
    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_MENU")
    private Menu menu;

//-----------------------------------------------------CONSTRUCTOR---------------------------------------------------------------
    public Reserva() {
    }
    public Reserva(Date horafecha, String direccion, int utensilios, float deposito, String formaDePago, String visible, Cliente cliente, Menu menu) {
        this.horafecha = horafecha;
        this.direccion = direccion;
        this.utensilios = utensilios;
        this.deposito = deposito;
        this.formaDePago = formaDePago;
        this.total = menu.getPrecio();
        this.visible = visible;
        this.cliente = cliente;
        this.menu = menu;
    }
    public Reserva(Date horafecha, String direccion, int utensilios, float deposito, String formaDePago, String visible) {
        this.horafecha = horafecha;
        this.direccion = direccion;
        this.utensilios = utensilios;
        this.deposito = deposito;
        this.formaDePago = formaDePago;
        this.visible = visible;
    }
//-----------------------------------------------------GET/SET---------------------------------------------------------------
    public int getIdReserva() {
        return idReserva;
    }
    public Date getHorafecha() {
        return horafecha;
    }
    public String getDireccion() {
        return direccion;
    }
    public int getUtensilios() {
        return utensilios;
    }
    public String getFormaDePago() {
        return formaDePago;
    }
    public float getDeposito() {
        return deposito;
    }
    public float getTotal() {
        return total;
    }
    public String getVisible() {
        return visible;
    }
    public Cliente getCliente() {
        return cliente;
    }
    public Menu getMenu() {
        return menu;
    }
    
//-----------------------------------------------------SET---------------------------------------------------------------
    public void setIdReserva(int idReserva) {
        this.idReserva = idReserva;
    }
    public void setHorafecha(Date horafecha) {
        this.horafecha = horafecha;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public void setUtensilios(int utensilios) {
        this.utensilios = utensilios;
    }
    public void setFormaDePago(String formaDePago) {
        this.formaDePago = formaDePago;
    }
    public void setDeposito(float deposito) {
        this.deposito = deposito;
    }
    public void setTotal(float total) {
        this.total = total;
    }
    public void setVisible(String visible) {
        this.visible = visible;
    }
    public void setMenu(Menu menu) {
        this.menu = menu;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    } 
    
    
}
